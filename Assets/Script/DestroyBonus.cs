﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBonus : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //heal = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, 5);
    }
    public delegate void SomeAction(float healthFill);
    public event SomeAction tr;
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            tr?.Invoke(0.15f);
            Destroy(gameObject);
        }
    }

}
