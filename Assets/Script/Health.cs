﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    private float _timeLeft  = 2f;
    private float fill = 1f;
    public Image _theHealthBar;

    public GameObject bonus;
    public float minDelay, maxDelay;
    private float nextLaunchTime;

    public void SetFill(float newFill)
    {
        fill += newFill;
    }

    void Start()
    {
        //DestroyBonus.singleton
        _theHealthBar = GetComponent<Image>();
    }

    void Update()
    {
        _timeLeft -= Time.deltaTime;
        if(_timeLeft < 0)
        {
            fill -= 0.1f;
            _theHealthBar.fillAmount = fill;
            _timeLeft = Random.Range(2, 6);
        }
        if(fill <= 0)
        {
            Application.LoadLevel("Menu");
        }
        if (Time.time > nextLaunchTime)
        {
            float xSize = transform.localScale.x / 2;
            Vector3 bonusPosition = new Vector3(
                Random.Range(minDelay, maxDelay),
                0.45f,
                transform.position.z);
            nextLaunchTime = Time.time + Random.Range(1, 10);
            Instantiate(bonus, bonusPosition, Quaternion.Euler(-90f, 0, 0));
        }
    }
}
