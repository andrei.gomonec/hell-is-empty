﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Move : MonoBehaviour
{
    private float _speed = 5;
    private Rigidbody playerRB;
    private float _walkTime = 0.5f, _walkCooldown = 1.5f;
    public Transform cam;
    private float distance;

    public Text scoreText;
    private float score = 0;
    private float _timeLeft = 1f;

    private Vector3 _targetPoint;

    //private MoveState _moveState = MoveState.Idle;
    private Animator _animatorController;
    public AudioSource _run;
    
    private float centr;

    //Touch controller
    private bool directionChosen;
    private bool isLeft;

    public test test;

    void Start() // initializing components;
    {
        playerRB = GetComponent<Rigidbody>();
        _animatorController = GetComponent<Animator>();
        distance = playerRB.position.z - cam.position.z;

        test = GetComponent<test>();

        //the width of the screen to control;
        centr = Screen.width / 2;
    }

    void FixedUpdate() // the player's movement;
    {
        if(_walkTime > 0)
        {
            playerRB.velocity = playerRB.transform.forward * _speed;
            cam.position = new Vector3(cam.position.x, cam.position.y, playerRB.position.z - distance);
            _walkTime -= Time.deltaTime;
            _speed -= 0.035f;
            _animatorController.Play("run");
            _timeLeft -= Time.deltaTime;
            if (_timeLeft < 0)
            {
                score += 1;
                scoreText.text = "SCORE: " + score;
                _timeLeft = 1f;
            }

            
        }

        if(_walkTime <= 0)
        {
            _animatorController.Play("IDLE");
        }
    }

    private void Update() // player management 
    {
        if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
        {
#if !UNITY_EDITOR
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    directionChosen = false;
                    break;

                case TouchPhase.Moved:
                    break;

                case TouchPhase.Ended:
                    if (touch.position.x > centr) isLeft = false;
                    if (touch.position.x < centr) isLeft = true;
                    directionChosen = true;
                    break;
            }
#endif

#if UNITY_EDITOR
            if (Input.mousePosition.x < centr)
            {
                directionChosen = true;
                isLeft = true;
            }

            if (Input.mousePosition.x > centr)
            {
                directionChosen = true;
                isLeft = false;
            }
#endif

            if (directionChosen == true && isLeft == false)
            {
                _targetPoint = new Vector3(test.rightBorder1, transform.position.y, transform.position.z);
                directionChosen = false;
            }
            else if (directionChosen == true && isLeft == true) 
            {
                _targetPoint = new Vector3(test.leftBorder1, transform.position.y, transform.position.z);
                directionChosen = false;
            }
            
            _walkTime = _walkCooldown;
            _speed = 5f;
            _run.Play();
        }
        transform.position = new Vector3(Mathf.Lerp(transform.position.x, _targetPoint.x, 4f * Time.deltaTime), 
                                         transform.position.y, 
                                         transform.position.z);
    }


    /*enum MoveState
    {
        Idle,
        Walk,
    }*/

}
