﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunks : MonoBehaviour
{
    public Transform point;
    public GameObject _platform;
    public List<GameObject> _platforms;

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Player")
        {
            GameObject insPlatfor =  Instantiate(_platform, point.position, Quaternion.Euler(0, -90, 0));
            _platforms.Add(insPlatfor);
        }
    }
}
